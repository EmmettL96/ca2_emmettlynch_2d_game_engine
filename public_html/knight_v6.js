/* global now */

//Some code borrowed from Raining Chain
//Emmett Lynch - D001853366
//I also borrowed code the first Snake game we did in class
//W3Schools
//Sprites used from: Ghosts and Goblins, Pokemon Emerald, Castlevania

var ctx = document.getElementById("ctx").getContext("2d");
var requestAnimationFrame = window.requestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.msRequestAnimationFrame,
    fpsElement = document.getElementById('fps');
          
var soundAndMusicElement = document.getElementById("knight-sound-and-music"),
    musicElement = document.getElementById("knight-music"),
    musicCheckboxElement = document.getElementById("knight-music-checkbox"),
    soundCheckboxElement = document.getElementById("knight-sound-checkbox"),
    musicOn = musicCheckboxElement.checked;
    
    
musicElement.volume = 0.1;

//image object that stores the sprites
//initializing the images into the script
var Img = {};
Img.player_sprite = new Image();
Img.player_sprite.src = "images/knight_player.png";
Img.knight_spriteSheet = new Image();
Img.knight_spriteSheet.src = "images/knight_spritesheet.png";
Img.monster_sprite = new Image();
Img.monster_sprite.src = "images/ghost_2.png";
Img.lava_sprite = new Image();
Img.lava_sprite.src = "images/lava.png";
Img.ladder_sprite = new Image();
Img.ladder_sprite.src = "images/ladder.png";
Img.treasure_sprite = new Image();
Img.treasure_sprite.src = "images/Money_Bag.png";
Img.map1 = new Image();
Img.map1.src = "images/level1_map.png";
Img.map2 = new Image();
Img.map2.src = "images/level2_map.png";
Img.map3 = new Image();
Img.map3.src = "images/level3_map.png";
Img.endScreen = new Image();
Img.endScreen.src = "images/end_screen.png";
Img.life = new Image();
Img.life.src = "images/heart.png";

//var now = Date.now();

var ring = new Audio();
ring.src = "soundEffects/Ring.mp3";
ring.volume = 0.05;

var boing = new Audio();
boing.src = "soundEffects/Boing.mp3";
boing.volume = 0.05;

var pop = new Audio();
pop.src = "soundEffects/tv.mp3";
pop.volume = 0.05;

var splash = new Audio();
splash.src = "soundEffects/splash.ogg";
splash.volume = 0.2;
 
var canvasHeight = 500;
var canvasWidth = 500;
 
//the player is declared as an object with attribrutes
var player = {
        x:50,
        y:450,
        width: 30,
        height: 40,
        health: 50,
        img: Img.player_sprite,
        pressingDown: false,
        pressingUp: false,
        pressingLeft: false,
        pressingRight: false,
        pressingRestart: false
};

var nextLevelDoor = {
    x: 450,
    y: 50,
    width: 18,
    height: 45,
    img: Img.ladder_sprite
};

//constants
var collectedTreasure = false;
var hurtByGhost = false;
var hurtByLava = false;
var pausedGame = false;
var PAUSE_CHECK_INTERVAL = 200;
var restartGame = false;
var time = 0;
var monsterList = {};
var treasureList = {};
var lavaPool = {};
var toastList = {};
var level = 0;
var score = 0;
var highScore = 0;
var gameCompleted = false;
var lives = 3;
var gameOver = false;

//time
var lastAnimationFrameTime = 0,
    lastFpsUpdateTime = 0,
    fps = 60;

//values are passed into the monster function via constructor
//The values are then added into a monster list array
Monster = function(id, x, y, spdX, spdY, width, height)
{
    var monster1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        width: width,
        height: height,
        id: id,
        img: Img.monster_sprite
        
                    };
    monsterList[id] = monster1;
},

//similar to monster function
Treasure = function(id, x, y, spdX, spdY, width, height)
{
    var treasure1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        name:'E',
        id: id,
        width: width,
        height: height,
        img: Img.treasure_sprite
                    };
    treasureList[id] = treasure1;
},

//similar to monster function
LavaArea = function(id, x, y, spdX, spdY, width, height)
{
    var lava1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        name:'E',
        id: id,
        width: width,
        height: height,
        img: Img.lava_sprite
                    };
    lavaPool[id] = lava1;
},

toasts = function(id, x, y, text)
{
    var toast1 = {
        id: id,
        x: x,
        y: y,
        text: text
    };
    
    toastList[id] = toast1;
    
},
        
startMusic = function()
{
    var MUSIC_DELAY = 1000;
    setTimeout(function()
    {
        if(musicCheckboxElement.checked)
        {
            musicElement.play();
        }
        pollMusic();
        
    }, MUSIC_DELAY);
},
        
pollMusic = function()
{
    var POLL_INTERVAL = 500,
        SOUNDTRACK_LENGTH = 132,
        timerID;

    timerID = setInterval( function()
    {
        if(musicElement.currentTime > SOUNDTRACK_LENGTH)
        {
            clearInterval(timerID); //stop polling
            restartMusic();
        }
    }, POLL_INTERVAL);
},
        
restartMusic = function()
{
    musicElement.pause();
    musicElement.currentTime = 0;
    startMusic();
},
        
calculateLives = function()
{
    var space = 28;
    for(var i = 0; i < lives; i++)
    {
        ctx.drawImage(Img.life, space, 10, 22, 22);
        //ctx.fillText("L", space, 10);
        space+=20;
    }
},
        
checkForGameOver = function()
{
    if(lives === 0)
    {
        gameOver = true;
        endScreen();
    }
},

//keyboard input
document.onkeydown = function(event)
{
    if(event.keyCode === 68)
    {
        player.pressingRight = true;
    }
    
    else if(event.keyCode === 83)
    {
        player.pressingDown = true;
    }
    
    else if(event.keyCode === 65)
    {
        player.pressingLeft = true;
    }
    
    else if(event.keyCode === 87)
    {
        player.pressingUp = true;
    }
    
    else if(event.keyCode === 80)
    {
        pausedGame = !pausedGame;
        pop.play();
    }
    
    else if(event.keyCode === 82)
    {
        restartGame = !restartGame;
    }
        
},

document.onkeyup = function(event)
{
    if(event.keyCode === 68)
    {
        player.pressingRight = false;
    }
    
    else if(event.keyCode === 83)
    {
        player.pressingDown = false;
    }
    
    else if(event.keyCode === 65)
    {
        player.pressingLeft = false;
    }
    
    else if(event.keyCode === 87)
    {
        player.pressingUp = false;
    }
    
    else if(event.keyCode === 82)
    {
        player.pressingRestart = false;
    }
},

//updates the players x or y movement via keyboard inputs
updatePlayerPosition = function()
{
    if(player.pressingRight)
    {
        player.x += 4;
    }
    
    if(player.pressingLeft)
    {
        player.x -= 4;
    }
    
    if(player.pressingDown)
    {
        player.y += 4;
    }
    
    if(player.pressingUp)
    {
        player.y -= 4;
    }
},

//The game enitities are passed into this function to update their x y position
//then they are drawn according to the values passed into the constructor
updateEntity = function(shape)
{
    updateEntityPosition(shape);
    drawEntity(shape);
},

//updating the x y positions of only entities that have certain ids
updateEntityPosition = function(shape)
{
    
    if(shape.id === 'M1')
    {
        if(level === 0)
        {
            shape.x += shape.spdX;
        
            if(shape.x < 300 || shape.x > 400)
            {
                shape.spdX = -shape.spdX;
            }
        }
        
        if(level === 2)
        {
            shape.y += shape.spdY;
        
            if(shape.y < 200 || shape.y > 400)
            {
                shape.spdY = -shape.spdY;
            }
        }
        
        if(level === 3)
        {
            
            shape.x += shape.spdX;
            shape.y += shape.spdY;
            
            if(shape.x < 0 || shape.x > canvasWidth)
            {
                shape.spdX = -shape.spdX;
            }
            
            if( shape.y < 0 || shape.y > canvasHeight)
            {
                shape.spdY = -shape.spdY;
            }
           
            
        }
        
    }
    
    if(shape.id === 'M2')
    {
        if(level === 2)
        {
            shape.x += shape.spdX ;
            
            if(shape.x < 40 || shape.x > 450)
            {
                shape.spdX = -shape.spdX;
            }
        }
        
        if(level === 3)
        {
            shape.x += shape.spdX;
            
            if(shape.x < 20 || shape.x > 450)
            {
                shape.spdX = -shape.spdX;
            }
        }
    }
    
    if(shape.id === 'M3')
    {
        if(level === 3)
        {
            shape.x += shape.spdX;
            shape.y += shape.spdY;
            
            if(shape.x < 0 || shape.x > canvasWidth)
            {
                shape.spdX = -shape.spdX;
            }
            
            if(shape.y < 0 || shape.y > canvasHeight)
            {
                shape.spdY = -shape.spdY;
            }
        }
    }
    
    if(shape.id === 'M4')
    {
        if(level === 3)
        {
            shape.x += shape.spdX;
            if(shape.x < 0 || shape.x > canvasWidth)
            {
                shape.spdX = -shape.spdX;
            }
        }
    }
    
},

drawEntity = function(shape)
{
    ctx.save();
    var x = shape.x-shape.width/2;
    var y = shape.y-shape.width/2;
    
    //draws the image to correct proportions of width and height of the entity
    ctx.drawImage(shape.img, 0, 0, shape.img.width, shape.img.height,
            x, y, shape.width, shape.height);
    ctx.restore();
},

drawToast = function(shape)
{
    if(shape.id === 'Ts3')
    {
        ctx.save();
        ctx.font="14px Spirax";
        //font-family: 'Spirax', cursive;
    
        //W3schools
        var gradient = ctx.createLinearGradient(0,0,canvasWidth, 0);
        gradient.addColorStop("0.5", "yellow");
        gradient.addColorStop("1.0", "orange");
    
        ctx.fillStyle = gradient;
    
        ctx.fillText(shape.text, shape.x, shape.y);
        ctx.restore();
    }
    else if(shape.id !== 'Ts3')
    {
        ctx.save();
        ctx.font="30px Spirax";
        //font-family: 'Spirax', cursive;
    
        //W3schools
        var gradient = ctx.createLinearGradient(0,0,canvasWidth, 0);
        gradient.addColorStop("0.5", "yellow");
        gradient.addColorStop("1.0", "orange");
    
        ctx.fillStyle = gradient;
    
        ctx.fillText(shape.text, shape.x, shape.y);
        ctx.restore();
    }
    
},

drawBackground = function()
{
    
    ctx.drawImage(Img.map1,0,0);
    
    if(level === 2)
        ctx.drawImage(Img.map2,0,0);
    
    
    if(level === 3)
        ctx.drawImage(Img.map3,0,0);
    
    if(gameCompleted)
        ctx.drawImage(Img.endScreen, 0, 0);
    
    if(gameOver)
        ctx.drawImage(Img.endScreen, 0, 0);
    
    
    
    
},

//returns the distance between two entities passed into the function
getDistanceBetweenPlayerandMonster = function(o1, o2)
{
    var vx = o1.x - o2.x;
    var vy = o1.y - o2.y;
    
    return Math.sqrt(vx * vx + vy * vy);
},


testCollision = function(o1, o2)
{
    var rec1 = {
        x : o1.x - o1.width/2,
        y : o1.y - o1.height/2,
        width: o1.width,
        height: o1.height
    };
    
    var rec2 = {
        x : o2.x - o2.width/2,
        y : o2.y - o2.height/2,
        width : o2.width,
        height : o2.height
    };
    
    return testCollisionForRectangles(rec1, rec2);
};

//check if rectangles are colliding with eachother
testCollisionForRectangles = function(rec1, rec2)
{
    return  rec1.x <= rec2.x + rec2.width
        &&  rec2.x <= rec1.x + rec1.width
        &&  rec1.y <= rec2.y + rec2.height
        &&  rec2.y <= rec1.y + rec1.height;
};

//stops player from leaving canvas
enableCanvasBoundary = function()
{
    if(player.x < player.width/2)
        player.x = player.width/2;
    
    if(player.x > canvasWidth - player.width/2)
        player.x = canvasWidth - player.width/2;
    
    if(player.y < player.height/2)
        player.y = player.height/2;
    
    if(player.y > canvasHeight- player.height/2)
        player.y = canvasHeight- player.height/2;
};

getFeedback = function()
{
    if(collectedTreasure)
        document.getElementById("feedback").innerHTML = "Collected treasure!";
        
    else if(hurtByLava)
        document.getElementById("feedback").innerHTML = "You tried to swim in lava!";
    
    else if(hurtByGhost)
        document.getElementById("feedback").innerHTML = "You got spooked by the ghost!";
    
    else
        document.getElementById("feedback").innerHTML = "Collect the treasure!";     
};

determineHighScore = function()
{
    if(score >= highScore)
    {
        highScore = score;
    }
    
    document.getElementById('hs').innerHTML = highScore;
};

calculateFps = function(now)
{
    var fps = 1/(now - lastAnimationFrameTime) * 1000;
    if(now - lastFpsUpdateTime > 1000)
    {
        lastFpsUpdateTime = now;
        fpsElement.innerHTML = fps.toFixed(0) + ' fps';
    }
    
    return fps;
};

update = function(now){
        if(pausedGame)
        {
            ctx.font="30px Spirax";
            var gradient = ctx.createLinearGradient(0,0,canvasWidth, 0);
            gradient.addColorStop("0.5", "yellow");
            gradient.addColorStop("1.0", "orange");

            ctx.fillStyle = gradient;
            ctx.fillText("paused", canvasWidth - 110 ,40);
            
            setTimeout(function(){
                musicElement.pause();
                requestAnimationFrame(update);
            }, PAUSE_CHECK_INTERVAL);
        }
        else
        {
            
            fps = calculateFps(now);
            lastAnimationFrameTime = now;
            
            
            
            if(restartGame)
            {
                restartGame = false;
                musicElement.play();
                resumeGame();
            }
        
            musicCheckboxElement.addEventListener("change", function(e)
            {
                musicOn = musicCheckboxElement.checked;
                if(musicOn)
                {
                    musicElement.play();
                }
                else
                {
                    musicElement.pause();
                }
            });


            time++;

            drawBackground();


            //looping through array
            for(var i in treasureList)
            {
                updateEntity(treasureList[i]);

                var collision = testCollision(player, treasureList[i]);
                if(collision)
                {
                    ring.play();
                    score += 1;
                    delete treasureList[i];
                    collectedTreasure = true;
                    document.getElementById('demo').innerHTML = score;
                    determineHighScore();
                }
            }

            //looping through array
            for(var i in monsterList)
            {
                updateEntity(monsterList[i]);

                var collision = testCollision(player, monsterList[i]);
                if(collision)
                {
                    lives--;
                    hurtByLava = false;
                    hurtByGhost = true;
                    resumeGame();  //resets the game
                }
            }

            //looping through array
            for(var i in lavaPool)
            {
                updateEntity(lavaPool[i]);

                var collision = testCollision(player, lavaPool[i]);
                if(collision)
                {
                    lives--;
                    splash.play();
                    hurtByGhost = false;
                    hurtByLava = true;
                    resumeGame(); //resets the game
                }
            }

            //looping through array
            for(var i in toastList)
            {
                drawToast(toastList[i]);

                if(time === 400)
                {
                    delete toastList[i];
                }
            }

            var goal = testCollision(player, nextLevelDoor);
            if(goal)
            {
                boing.play();
                level += 1;
                console.log(level);

                if(level === 2)
                {
                    startLevel2(); //resets the game
                }

                if(level === 3)
                {
                    startLevel3();
                }

                if(level >= 4)
                {
                    gameCompleted = true;
                    endScreen();
                }

            }

            enableCanvasBoundary();

            getFeedback();

            updatePlayerPosition();

            calculateLives();
            
            checkForGameOver();

            //if the game is completed the player and door are not drawn
            if(!gameCompleted && !gameOver)
            {
                drawEntity(player);
                drawEntity(nextLevelDoor);
            }

            requestAnimationFrame(update);
            }
};

//initialize the game
startGame = function()
{
    startMusic();
    
    player.x = 50;
    player.y = 450;
    nextLevelDoor. x = 450;
    nextLevelDoor. y = 50;
    score = 0;
    lives = 3;
    time = 0;
    gameCompleted = false;
    gameOver = false;
    document.getElementById('demo').innerHTML = score;
    document.getElementById('hs').innerHTML = highScore;
    collectedTreasure = false;
    level = 0;
    monsterList = {};
    
    //calling the functions then passing values to determine the attributes of the entity
    Monster('M1', 310, 85, 2, 15, 30, 30);
    
    toasts('Ts1', 30, 50,"Welcome to Knight Dash!");
    toasts('Ts2', 30, 110,"Press 'WASD' to move and P to pause!");
    
    //calling the functions then passing values to determine the attributes of the entity
    Treasure('T1', 250, 250, 0, 0, 30, 30);
    Treasure('T2', 270, 270, 0, 0, 30, 30);
    Treasure('T3', 290, 290, 0, 0, 30, 30);
    Treasure('T4', 420, 280, 0, 0, 30, 30);
    Treasure('T5', 305, 85, 0, 0, 30, 30);
    
    //calling the functions then passing values to determine the attributes of the entity
    LavaArea('L1', 110, 180, 0, 0, 120, 120);
    LavaArea('L2', 90, 340, 0, 0, 100, 100);
    LavaArea('L3', 420, 340, 0, 0, 75, 75);
    LavaArea('L4', 360, 300, 0, 0, 75, 75);
},
        
resumeGame = function()
{
    ctx.clearRect(0,0, canvasWidth, canvasHeight);
    player.x = 50;
    player.y = 450;
    nextLevelDoor. x = 450;
    nextLevelDoor. y = 50;
    score = 0;
    gameOver = false;
    gameCompleted = false;
    document.getElementById('demo').innerHTML = score;
    document.getElementById('hs').innerHTML = highScore;
    collectedTreasure = false;
    level = 0;
    monsterList = {};
    toastList = {};
    
    //calling the functions then passing values to determine the attributes of the entity
    Monster('M1', 310, 85, 2, 15, 30, 30);
    
    //calling the functions then passing values to determine the attributes of the entity
    Treasure('T1', 250, 250, 0, 0, 30, 30);
    Treasure('T2', 270, 270, 0, 0, 30, 30);
    Treasure('T3', 290, 290, 0, 0, 30, 30);
    Treasure('T4', 420, 280, 0, 0, 30, 30);
    Treasure('T5', 305, 85, 0, 0, 30, 30);
    
    //calling the functions then passing values to determine the attributes of the entity
    LavaArea('L1', 110, 180, 0, 0, 120, 120);
    LavaArea('L2', 90, 340, 0, 0, 100, 100);
    LavaArea('L3', 420, 340, 0, 0, 75, 75);
    LavaArea('L4', 360, 300, 0, 0, 75, 75);
    
},

startLevel2 = function()
{
    ctx.clearRect(0,0, canvasWidth, canvasHeight);
    player.x = 50;
    player.y = 450;
    
    monsterList = {};
    treasureList = {};
    Monster('M1', 310, 200, 4, 4, 30, 30);
    //Monster('M2', 270, 60, 4, 4, 30, 30);
    
    Treasure('T1', 85, 280, 0, 0, 30, 30);
    Treasure('T2', 250, 270, 0, 0, 30, 30);
    Treasure('T3', 320, 290, 0, 0, 30, 30);
    Treasure('T4', 325, 70, 0, 0, 30, 30);
    
    LavaArea('L1', 230, 200, 0, 0, 85, 85);
    LavaArea('L2', 90, 340, 0, 0, 85, 85);
    LavaArea('L3', 145, 300, 0, 0, 85, 85);
    LavaArea('L4', 180, 240, 0, 0, 85, 85);
    
},


startLevel3 = function()
{
    ctx.clearRect(0,0, canvasWidth, canvasHeight);
    player.x = 50;
    player.y = 450;
    
    monsterList = {};
    treasureList = {};
    lavaPool = {};
    
    Treasure('T1', 190, 255, 0, 0, 30, 30);
    Treasure('T2', 320, 255, 0, 0, 30, 30);
    Treasure('T3', 75, 300, 0, 0, 30, 30);
    Treasure('T4', 75, 110, 0, 0, 30, 30);
    
    LavaArea('L1', canvasWidth/2, canvasHeight/2, 0, 0, 95, 95);
    
    Monster('M1', 250, 135, 5, 5, 30, 30);
    //Monster('M2', 65, 145, 5, 5, 30, 30);
    //Monster('M3', 475, 485, 5, 5, 30, 30);
    //Monster('M4', 145, 35, 5, 5, 30, 30);
    
    
},
    
/*    
gameOverScreen = function()
{
    musicElement.pause();
    ctx.clearRect(0,0, canvasWidth, canvasHeight);
    nextLevelDoor.x = 0;
    nextLevelDoor.y = 0;
    monsterList = {};
    treasureList = {};
    lavaPool = {};
    
    toasts('Ts2', canvasWidth/7, 145, "You died!");
    toasts('Ts3', canvasWidth/5, 170, "Press R to restart.");
},*/

endScreen = function()
{
    musicElement.pause();
    ctx.clearRect(0,0, canvasWidth, canvasHeight);
    nextLevelDoor.x = 0;
    nextLevelDoor.y = 0;
    monsterList = {};
    treasureList = {};
    lavaPool = {};
    
    toasts('Ts1', canvasWidth/7, 120,"Thank you for playing!");
    toasts('Ts2', canvasWidth/7, 145, "You managed to score " + score + " points");
    toasts('Ts3', canvasWidth/5, 170, "Press R to restart.");
},

startGame();

update();